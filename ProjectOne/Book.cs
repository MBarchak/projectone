﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectOne
{
    class Book
    {
        //Book Properties

        public string Name { get; set; }
        public DateTime CirculationStart { get; set; }
        public DateTime CirculationEnd { get; set; }
        public bool Archived { get; set; }
        public PriorityQueue Queue { get; set; }


        //constructor with string parameter
        public Book(string name)
        {
            Name = name;
            Archived = false;   //initial not archived
            Queue = new PriorityQueue();  //initializes the queue
        }
    }
}
