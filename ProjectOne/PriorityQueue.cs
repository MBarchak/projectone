﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectOne
{
    class PriorityQueue
    {


        /*
         * Two parallel lists are used to construct the priority queue:
         *
         * The first list stores a list of the employees (that have been pushed during circulation in the library class)
         * The first list stores the priority of the employees in matching order
         * 
         * ex: Employees[0] stores the first employee and Priority[0] stores the first employee's priority
         * 
         * When an employee is removed from the list, so is their priority, so as to prevent a misalignment in the two lists
         */
        public List<Employee> Employees { get; set; }   //the first parallel list
        public List<int> Priority { get; set; }         //the second parallel list

        //used to avoid an error in the initial book circulation
        public bool FirstCirculation { get; set; }

        //Constructor
        public PriorityQueue()
        {
            //intializes the lists and the sets the boolean to true

            Employees = new List<Employee>();
            Priority = new List<int>();
            FirstCirculation = true;
        }

        //Push function: adds the passed employee onto the employee list
        public void Push(Employee emp)
        {
            Employees.Add(emp);
        }

        //Pop function: removes and returns the popped employee
        public Employee Pop()
        {
            ////redertmines the priorities of each employee
            //DeterminePriority();

            int index = GetNextEmployee();  //obtain next employee
            if (index == -1)    //no more employees
                return null;

            Employee temp = Employees[index];   //temp version of the employee
            Employees.RemoveAt(index);  //removes the employee
            Priority.RemoveAt(index);   //removes the employee's priority number

            return temp;    //returns the popped employee
        }

        //checks to see if the employee list is empty
        public bool IsEmpty()   
        {
            if (Employees.Count == 0) //if there are no employees left in queue
                return true;
            else
                return false;
        }

        ////Determines the Priority of each employee - called before the next book owner is decided
        //private void DeterminePriority()
        //{
        //    //recalculates all of the employee's priorities
        //        for (int i = 0; i < Employees.Count; i++)
        //        {
        //            Priority[i] = Employees[i].WaitingTime - Employees[i].RetenTime;    //Priority = Waiting Time - Retention Time
        //        }
               
                
            
        //}

        //obtains the index of the employee with the highest priority
        private int GetNextEmployee()
        {
            
            int index = -1;
            int max = int.MinValue; //The Integer Minimum is used in case only a negative priority is left (as opposed to starting the max at 0)

            for (int i = 0; i < Priority.Count; i++)
            {
                if (Priority[i] > max)
                {
                    max = Priority[i];
                    index = i;
                }
            }

            //this is a work-around for the very first time a book is passed to someone
            //if the boolean is true and the index is the minimum possible integer (failsaife for subsequent priority queues)
            //then the index is set to represent the first employee instead of a null value
            if (FirstCirculation && index == -1)
            {
                index = 0;
            }
            FirstCirculation = false;
            return index;
        }
    }
}
