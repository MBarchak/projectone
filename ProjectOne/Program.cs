using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectOne
{
    class Program
    {
        static void Main(string[] args)
        {
            /*  Matthew Barchak
             * CS303 Project #1
             * Project: 1B
             * 
             * NOTE: Only the .cs files were included in the zipped archive, but the entire solution created in Visual Studios is available for cloning/downloading at:
             * https://gitlab.com/MBarchak/projectone
             * 
             */




            Library library = new Library();
            library.Add_Book("Software Engineering");
            library.Add_Book("Chemistry");

            library.Add_Employee("Adam");
            library.Add_Employee("Sam");
            library.Add_Employee("Ann");

            library.Circulate_Book("Chemistry", new DateTime(2015, 3, 1));
            library.Circulate_Book("Software Engineering", new DateTime(2015, 4, 1));

            library.Pass_On("Chemistry", new DateTime(2015, 3, 5));
            library.Pass_On("Chemistry", new DateTime(2015, 3, 7));
            library.Pass_On("Chemistry", new DateTime(2015, 3, 15));

            library.Pass_On("Software Engineering", new DateTime(2015, 4, 5));
            library.Pass_On("Software Engineering", new DateTime(2015, 4, 10));
            library.Pass_On("Software Engineering", new DateTime(2015, 4, 10));

            Console.ReadLine();
        }
    }
}
