﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectOne
{
    class Library
    {
        //Library Properties

        public List<Book> Circulation { get; set; } //books in circulation
        public List<Book> Archived { get; set; }    //archived books
        public List<Employee> Employees { get; set; }   //employee list

        //Constructor
        public Library()
        {
            //initialize the lists

            Circulation = new List<Book>(); 
            Archived = new List<Book>();
            Employees = new List<Employee>();
        }

        //Method that adds a new book to the circulation list
        public void Add_Book(string name)
        {
            //uses two lambda expressions to check both lists for the book being added
            if (Circulation.Find(c => c.Name == name) != null || Archived.Find(c => c.Name == name) != null)
            {
                Console.WriteLine("This book is already in circulation");
                return;
            }

            //Creates a book instance and adds it into the circulation list
            Circulation.Add(new Book(name));
        }

        //Adds a new employee to the employee list
        public void Add_Employee(string name)
        {
            //creates an employee instance and adds it into the  list
            Employees.Add(new Employee(name));
            
        }

        //The intial circulation of a book
        public void Circulate_Book(string name, DateTime date)
        {
            Employee e;

            //find the index of the book in the list with the passed name
            int index = FindBook(name);

            //set the circulation date
            Circulation[index].CirculationStart = date;


            //puts every person in the system into the book's queue
            foreach (Employee emp in Employees)
            {
                //push the employee
                Circulation[index].Queue.Push(emp);

                //add in their current priority
                Circulation[index].Queue.Priority.Add(emp.WaitingTime - emp.RetenTime);
            }

            UpdatePriorities(name, date, index);

            //pops the first person off of the queue, the first book owner
            e = Circulation[index].Queue.Pop();
            //resets the new owner's waiting time
            //e.WaitingTime = 0;
            
            //if the employee exists
            if (e != null)
            {
                //sets the vars containing info about the book acquisition

                e.CurrentBook = Circulation[index];
                e.DateAcquired = date;
            }

            UpdateOwner(e, index);

        }

        //Method when a book is passed from a previous owner to the next
        public void Pass_On(string name, DateTime date)
        {
            Employee e;
            
            //find the index of the passed book
            int index = FindBook(name);

            UpdatePriorities(name, date, index);

            //if the current book's queue is empty
            if (Circulation[index].Queue.IsEmpty())
            {
                //updates the archived bool for the book
                Circulation[index].Archived = true;

                //sets the circulation end date for the book
                Circulation[index].CirculationEnd = date;

                //adds the book to the archived list
                Archived.Add(Circulation[index]);

                //removes the book from the circulation list
                Circulation.Remove(Circulation[index]);

                //breaks out of the function
                return;
            }

            

            // Pops the next person off of the book's queue and gives them the book
            e = Circulation[index].Queue.Pop();

            //if e exists
            if (e != null)
            {
                //sets the vars containing info about the book acquisition

                e.CurrentBook = Circulation[index];
                e.DateAcquired = date;
            }

            UpdateOwner(e, index);
        }

        //finds the index of a book given its name
        private int FindBook(string str)
        {

            //a lambda expression is used to find the book in the Circulation list that has a matching name
            Book b = Circulation.Find( c => c.Name == str);

            //Searches through the list and obtains the index of the book in the list
            int index = Circulation.IndexOf(b);
            return index;
        }

        //loops through the employee list and updates the queue's employee information
        private void UpdatePriorities(string name, DateTime date, int index)
        {

            //loop that updates the employees waiting and retention times, both in the library's list, and in the queue's list
            for (int i = 0; i < Employees.Count; i++)
            {


                //loops through every employee and updates their retention/waiting times
                //checks for the employee currently holding the book in question
                if (Employees[i].CurrentBook != null)
                {
                    //if the book exists, the name is checked for equivalence
                    if (Employees[i].CurrentBook.Name == name)
                    {

                        //increases the retention time by the difference in days:
                        //The Subratct method is used, which subtracts two DateTime objects
                        //and returns a timespan, which has a property used: TotalDays
                        Employees[i].RetenTime += (int)date.Subtract(Employees[i].DateAcquired).TotalDays;
                        Employees[i].CurrentBook = null; //sets the value to null, the book is no longer in the employee's possesion

                    }
                }
                else
                    //Similar to above, but the waiting time is increased by the difference between the current date and
                    //the beginning circulation date for the book
                    if (Circulation[index].Queue.Employees.Contains(Employees[i]))
                        Employees[i].WaitingTime = (int)date.Subtract(Circulation[index].CirculationStart).TotalDays;

                //nested loop that compares each employee in the library list to the employees in the Queue
                //if their names match, the Queue's employee records are updated
                for (int j = 0; j < Circulation[index].Queue.Employees.Count; j++)
                {
                    if (Employees[i].Name == Circulation[index].Queue.Employees[j].Name)
                    {
                        Circulation[index].Queue.Employees[j].WaitingTime = Employees[i].WaitingTime;   //updated waiting time
                        Circulation[index].Queue.Employees[j].RetenTime = Employees[i].RetenTime;       //updated retention time
                        Circulation[index].Queue.Employees[j].CurrentBook = Employees[i].CurrentBook;
                    }
                }
            }


        }

        //for loop that updates the values of a book's owner's information
        private void UpdateOwner(Employee emp, int index)
        {
            for (int i = 0; i < Circulation[index].Queue.Employees.Count; i++)
            {
                if (emp.Name == Circulation[index].Queue.Employees[i].Name)
                {
                    Circulation[index].Queue.Employees[i].CurrentBook = emp.CurrentBook;
                    Circulation[index].Queue.Employees[i].DateAcquired = emp.DateAcquired;
                    Circulation[index].Queue.Employees[i].WaitingTime = emp.WaitingTime;
                }

            }
        }
    }
}
