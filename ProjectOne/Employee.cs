﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectOne
{
    class Employee
    {
        //Employee Properties

        public string Name { get; set; }        //name property
        public int WaitingTime { get; set; }    //waiting time property
        public int RetenTime { get; set; }      //retention time property

        //Two additional properties not mentioned in the project requirements
        //They monitor the employee's current book and the date they acquired it
        public Book CurrentBook { get; set; }
        public DateTime DateAcquired { get; set; }
        
        //constructor w/ string parameter
        public Employee(string n)
        {
            Name = n;

            //initial wait/reten time of 0
            WaitingTime = 0;
            RetenTime = 0;
            CurrentBook = null;
        }
    }
}
